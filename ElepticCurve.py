# -*- coding: utf-8 -*-

import collections

class Point:
    def __init__(self, point=(0,0),
                 Curve=collections.namedtuple('EllipticCurve', 'name p a b g n h')(
        'secp256k1',
        # Field characteristic.
        p=0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f,
        # Curve coefficients.
        a=0,
        b=7,
        # Base point.
        g=(0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798,
           0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8),
        # Subgroup order.
        n=0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141,
        # Subgroup cofactor.
        h=1
    )):
        self.point = point
        self.curve = Curve
        self.__Dict = self.getDict()


    def __add__(self, otherPoint):
        return Point(self.__addition(self.point, otherPoint.point))

    def __mul__(self, otherPoint):
        return Point(self.__multiplication(other, self.otherPoint))

    def __rmul__(self, otherPoint):
        return Point(self.__multiplication(otherPoint, self.point))

    def __sub__(self, otherPoint):
        return Point(self.__addition(self.point, self.__invert(otherPoint.point)))

    def getDict(self):
        a = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ "
        d = {}
        # P = 17*Point((0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798,
        #    0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8))
        for index, item in enumerate(a):
            d[self.__multiplication((index+17), self.point)] = item

        return d

    def __gcdExtended(self, k, p):
        """Возвращает обратное k по модулю p.
        Эта функция возвращает число x удовлетворяющее условию (x * k) % p == 1.
        k не должно быть равно 0 и p должно быть простым.
        """
        if k == 0:
            raise ZeroDivisionError('деление на 0')

        if k < 0:
            # k ** -1 = p - (-k) ** -1  (mod p)
            return p - self.__gcdExtended(-k, p)

        # Раширенный алгоритм Евклида.
        s, old_s = 0, 1
        t, old_t = 1, 0
        r, old_r = p, k

        while r != 0:
            quotient = old_r // r
            old_r, r = r, old_r - quotient * r
            old_s, s = s, old_s - quotient * s
            old_t, t = t, old_t - quotient * t

        gcd, x, y = old_r, old_s, old_t

        assert gcd == 1
        assert (k * x) % p == 1

        return x % p

    def isOnCurve(self, point):
        """Возвращает True если точка лежит на элиптической кривой."""
        if point is None:
            # None represents the point at infinity.
            return True
        x, y = point
        return (y * y - x * x * x - self.curve.a * x - self.curve.b) % self.curve.p == 0

    def __invert(self, point):
        """Инвертирует точку по оси y -point."""
        assert self.isOnCurve(point)

        if point is None:
            # -0 = 0
            return None

        x, y = point
        result = (x, -y % self.curve.p)

        assert self.isOnCurve(result)

        return result

    def __addition(self, point, point2):
        """Возвращает результат операции сложения point1 + point2 оперируя законами операции над группами."""

        assert self.isOnCurve(point)
        assert self.isOnCurve(point2)

        if point is None:
            return point2
        if point2 is None:
            return point

        x1, y1 = point
        x2, y2 = point2

        if x1 == x2 and y1 != y2:
            # point1 + (-point1) = 0
            return None

        if x1 == x2:
            # point == point2.
            m = (3 * x1 * x1 + curve.a) * self.__gcdExtended(2 * y1, curve.p)
        else:
            # точки не равны между собой point != point2.
            m = (y1 - y2) * self.__gcdExtended(x1 - x2, curve.p)

        x3 = m * m - x1 - x2
        y3 = y1 + m * (x3 - x1)
        result = (x3 % curve.p, -y3 % curve.p)
        assert self.isOnCurve(result)

        return result

    def __multiplication(self, k, point):
        """Возвращает k * точку используя дублирование и алгоритм сложения точек."""
        assert self.isOnCurve(point)

        # if k % curve.n == 0 or point is None:
        #     return None

        if k < 0:
            # k * point = -k * (-point)
            return self.__multiplication(-k, self.__invert(point))

        result = None
        addend = point

        while k:
            if k & 1:
                # Add.
                result = self.__addition(result, addend)

            # Double.
            addend = self.__addition(addend, addend)

            k >>= 1

        assert self.isOnCurve(result)

        return result

    def __getLetterPoint(self, letter):
        return list(self.__Dict.keys())[list(self.__Dict.values()).index(letter)]


    def encryptMsg(self, msg, publicKey, k):
        encryptMsg = ()
        for i in msg:

                encryptMsg += (self.__multiplication(k, curve.g), self.__addition(self.__getLetterPoint(i)
                                                                              , self.__multiplication(k, (publicKey))))

        return encryptMsg

    def decryptMsg(self, msg, privateKey):
        decryptMsg = ""
        for i in range(0,len(msg),2):
            try:
                decryptMsg += (self.__Dict.get(self.__addition(msg[i + 1], self.__invert(self.__multiplication(privateKey, msg[i])))))
            except TypeError:
                print("Неверный закрытый ключ")
                return None
        return decryptMsg


if __name__ == "__main__":

    EllipticCurve = collections.namedtuple('EllipticCurve', 'name p a b g n h')
    curve = EllipticCurve('test', p=751, a=-1, b=1, g=(1, 1), n=1, h=1)
    #
    # # Задание 1
    # msg = Point()
    # msg.curve = EllipticCurve('test', p=751, a=-1, b=1, g=(0, 1), n=1, h=1)
    # print(msg.curve)
    # print(msg.encryptMsg("у", (489,468), 6))
    # print(msg.encryptMsg("в", (489,468), 14))
    # print(msg.encryptMsg("е", (489,468), 5))
    # print(msg.encryptMsg("р", (489,468), 7))
    # print(msg.encryptMsg("о", (489,468), 12))
    # print(msg.encryptMsg("в", (489,468), 11))
    # print(msg.encryptMsg("а", (489,468), 4))
    # print(msg.encryptMsg("т", (489,468), 9))
    # print(msg.encryptMsg("ь", (489,468), 19))
    #
    # # Задание 2
    # encmsg = Point()
    # encmsg.curve = EllipticCurve('test', p=751, a=-1, b=1, g=(-1, 1), n=1, h=1)
    # print(encmsg.curve)
    # privateKey = 41
    # print(encmsg.decryptMsg((
    #     (283, 493), (314, 127), (425, 663), (561, 140), (568, 355), (75, 433), (440, 539), (602, 627), (188, 93),
    #     (395, 414), (179, 275), (25, 604), (72, 254), (47, 349), (72, 254), (417, 137), (188, 93), (298, 225),
    #     (56, 419), (79, 111)
    # ), privateKey))
    #
    # # Задание 3
    # P = Point((58, 139))
    # Q = Point((67, 667))
    # R = Point((82, 481))
    #
    # print((2*P + 3*Q - R).point)
    #
    # # Задание 4
    # P = 128*Point((62,372))
    # print(P.point)

    curve = EllipticCurve(
        'secp256k1',
        # Field characteristic.
        p=0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f,
        # Curve coefficients.
        a=0,
        b=7,
        # Base point.
        g=(0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798,
           0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8),
        # Subgroup order.
        n=0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141,
        # Subgroup cofactor.
        h=1
    )
    P = Point((0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798,
           0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8))
    a = Point
